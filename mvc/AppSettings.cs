﻿using System.Configuration;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC
{
    public static class AppSettings
    {
        public const string PRODUCTBUILDER_SECRETKEY = "productBuilderSecretKey";
        public const string PRODUCTBUILDER_ACCESSTOKEN = "productBuilderAccessToken";
        public const string PRODUCTBUILDER_ENDPOINT = "productBuilderEndpoint";

        public static string GetSetting(string settingName)
        {
            return ConfigurationManager.AppSettings.Get(settingName);
        }
    }
}