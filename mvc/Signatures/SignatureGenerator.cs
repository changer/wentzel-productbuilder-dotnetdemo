﻿using System.Security.Cryptography;
using System.Text;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Signatures
{
    public class SignatureGenerator
    {
        public string SecretKey
        {
            get
            {
                return AppSettings.GetSetting(AppSettings.PRODUCTBUILDER_SECRETKEY);
            }
        }

        public string AccessToken
        {
            get
            {
                return AppSettings.GetSetting(AppSettings.PRODUCTBUILDER_ACCESSTOKEN);

            }
        }

        public string Generate(string stringToSign)
        {
            string signature;

            byte[] secretkeyBytes = Encoding.UTF8.GetBytes(SecretKey);
            byte[] messageBytes = Encoding.UTF8.GetBytes(stringToSign);

            using (var hmac = new HMACSHA512(secretkeyBytes))
            {
                byte[] hashValue = hmac.ComputeHash(messageBytes);
                signature = System.Convert.ToBase64String(hashValue);
            }

            return signature;
        }
    }
}