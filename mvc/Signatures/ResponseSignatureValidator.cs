﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wentzel.ProductBuilder.DotNetDemo.MVC.Models;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Signatures
{
    public class ResponseSignatureValidator
    {
        public string LastExpectedSignature { get; private set; }
        private readonly SignatureGenerator SignatureGenerator;
        private readonly ProjectResponse ProjectResponse;

        public ResponseSignatureValidator(ProjectResponse projectResponse)
        {
            SignatureGenerator = new SignatureGenerator();
            ProjectResponse = projectResponse;
        }

        public bool IsValid(string signatureToValidate)
        {
            string stringToSign = string.Format("{0}{1}", ProjectResponse.ProjectID, ProjectResponse.TotalPrice);
            string expectedSignature = SignatureGenerator.Generate(stringToSign);
            
            LastExpectedSignature = expectedSignature;

            return signatureToValidate.Equals(expectedSignature, StringComparison.Ordinal);
        }
    }
}