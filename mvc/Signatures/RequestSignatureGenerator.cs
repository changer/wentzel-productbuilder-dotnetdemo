﻿using System;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Signatures
{
    public class RequestSignatureGenerator
    {
        private readonly SignatureGenerator SignatureGenerator;

        public RequestSignatureGenerator()
        {
            SignatureGenerator = new SignatureGenerator();
        }

        public Models.ProductBuilderRequest Generate()
        {
            string endpoint = AppSettings.GetSetting(AppSettings.PRODUCTBUILDER_ENDPOINT);

            string timestamp = DateTime.Now.ToString("o");

            var url = new Flurl.Url(endpoint);
            url.SetQueryParam("access_token", SignatureGenerator.AccessToken);
            url.SetQueryParam("timestamp", timestamp);

            string stringToSign = string.Format("{0}{1}{2}", endpoint, '\n', url.QueryParams);

            string signature = SignatureGenerator.Generate(stringToSign);

            url.SetQueryParam("signature", signature);

            return new Models.ProductBuilderRequest { AccessToken = SignatureGenerator.AccessToken, StringToSign = stringToSign, Signature = signature, Timestamp = timestamp, Url = url.ToString() };
        }
    }
}