﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Models
{
    public class ProductBuilderRequest
    {
        public string StringToSign { get; set; }
        public string Timestamp { get; set; }
        public string AccessToken { get; set; }
        public string Signature { get; set; }
        public string Url { private get; set; }
        public IHtmlString RawUrl
        {
            get
            {
                return new HtmlString(Url);
            }
        }
    }
}