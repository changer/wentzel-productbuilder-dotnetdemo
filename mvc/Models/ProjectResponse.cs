﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Models
{
    public class ProjectResponse
    {
        [JsonProperty(PropertyName="project_id")]
        public string ProjectID { get; set; }

        [JsonProperty(PropertyName = "total_price")]
        public string TotalPrice { get; set; }

        public string ActualSignature { get; set; }

        public string ExpectedSignature { get; set; }

        public bool IsValid { get; set; }

    }
}