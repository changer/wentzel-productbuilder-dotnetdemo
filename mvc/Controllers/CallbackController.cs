﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wentzel.ProductBuilder.DotNetDemo.MVC.Models;
using Wentzel.ProductBuilder.DotNetDemo.MVC.Signatures;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Controllers
{
    public class CallbackController : Controller
    {
        //
        // POST: /Callback/Success/
        [HttpPost]
        public ActionResult Success(string project, string signature)
        {
            var model = JsonConvert.DeserializeObject<ProjectResponse>(project);
            var responseSignatureValidator = new ResponseSignatureValidator(model);

            bool isValid = responseSignatureValidator.IsValid(signature);

            model.ExpectedSignature = responseSignatureValidator.LastExpectedSignature;
            model.ActualSignature = signature;
            model.IsValid = isValid;

            return View(model);
        }

    }
}
