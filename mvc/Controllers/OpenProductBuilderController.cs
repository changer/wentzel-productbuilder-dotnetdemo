﻿using System.Web.Mvc;

namespace Wentzel.ProductBuilder.DotNetDemo.MVC.Controllers
{
    public class OpenProductBuilderController : Controller
    {
        //
        // GET: /OpenProductBuilder/

        public ActionResult Index()
        {
            var SignatureGenerator = new Signatures.RequestSignatureGenerator();
            var model = SignatureGenerator.Generate();

            return View(model);
        }

    }
}
