# README #

This is an example solution to integrate the Wentzel Product Builder into your e-commerce system using .NET (C#, ASP.NET MVC).

### How do I get set up? ###

Download or clone the repository and open the solution in Visual Studio 2012 or higher.

Open web.config and change the appSettings accordingly.

Dependencies should be automatically downloaded using NuGet package restore.

### Example site ###

The master branch is automatically pushed to a windows azure website, so it always contains the latest stable version: <https://productbuilder-dotnetdemo.azurewebsites.net/>

### Who do I talk to? ###

This demo is provided as-is without any warranty. In case you would like to contribute, feel free to contact me.